package com.example.demo;

import java.time.LocalDate;

public class Util {

    // 二つの日付をDate型で受け取り、間にある平日の日数を返却
    public static int getWorkdaysBetween(LocalDate startDate, LocalDate endDate) {
        if (startDate == null || endDate == null || startDate.isAfter(endDate)) {
            throw new IllegalArgumentException("Invalid date range");
        }

        int workdays = 0;
        LocalDate currentDate = startDate;

        while (!currentDate.isAfter(endDate)) {
            if (currentDate.getDayOfWeek().getValue() <= 5) {
                workdays++;
            }
            currentDate = currentDate.plusDays(1);
        }
        return workdays;
    }

    // 任意の数値を与えて、素数かどうかを判定

    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }



}
