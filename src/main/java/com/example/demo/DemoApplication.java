package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	//add method that return string list of user's transaction data
	@GetMapping("/user")
	String user() {
		return "[{\"id\":1,\"name\":\"Samuel\",\"amount\":1000},{\"id\":2,\"name\":\"John\",\"amount\":2000}]";
	}





	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	// add comment

}